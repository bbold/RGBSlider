//
//  ViewController.swift
//  RGBSlider
//
//  Created by Mo Lotfi on 05/06/15.
//  Copyright (c) 2015 Mo Lotfi. All rights reserved.
//=================================================================================================================================================================QA========================================================
//why adding contsrains causes the viewbox to disappear
// embedding navigation bar changes push effect to slide in

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var redSlider: UISlider!
    @IBOutlet weak var greenSlider: UISlider!
    @IBOutlet weak var blueSlider: UISlider!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var colorSquare: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        redSlider.tintColor = UIColor.redColor()
        blueSlider.tintColor = UIColor.blueColor()
        greenSlider.tintColor = UIColor.greenColor()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        redSlider.value = defaults.floatForKey("red")
        greenSlider.value = defaults.floatForKey("green")
        blueSlider.value = defaults.floatForKey("blue")
        changeBackground()
        colorSquare.layer.borderColor = UIColor.blackColor().CGColor
        colorSquare.layer.borderWidth = 1

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        @IBAction func changeBackground() {
            let red = CGFloat(redSlider.value)
            let blue = CGFloat(blueSlider.value)
            let green = CGFloat(greenSlider.value)
            colorSquare.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
            textLabel.textColor = UIColor(red: blue, green: red, blue: green, alpha: 1)
            
//what is "shared" default object
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setFloat(redSlider.value, forKey: "red")
            defaults.setFloat(blueSlider.value, forKey: "blue")
            defaults.setFloat(greenSlider.value, forKey: "green")
            defaults.synchronize()
    }
//where should prepareForSegue go, Why is overrides here
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "openColor") {
            
//need to clarify this in plain english
            let newViewController = segue.destinationViewController as! UIViewController
            newViewController.view.backgroundColor = colorSquare.backgroundColor

        }
    }
    



}

